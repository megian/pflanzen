<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Vermehrung", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = 'propagation';

$fields = [
  ...$fields_defaults_main,
  [
    'type' => 'text',
    'name' => 'propagation',
    'label' => 'Vermehrung',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'propagationyear',
    'label' => 'Jahr',
    'size' => 8,
    'maxlength' => 8,
  ],
  [
    'type' => 'echo',
    'html' => '<p><b>Termin</b></p>',
  ],
  [
    'type' => 'select',
    'name' => 'beginweek',
    'label' => 'von',
    'optionsql' => 'SELECT id, text FROM week',
  ],
  [
    'type' => 'select',
    'name' => 'beginmonth',
    'optionsql' => 'SELECT id, text FROM month',
  ],
  [
    'type' => 'select',
    'name' => 'endweek',
    'label' => 'bis',
    'optionsql' => 'SELECT id, text FROM week',
  ],
  [
    'type' => 'select',
    'name' => 'endmonth',
    'optionsql' => 'SELECT id, text FROM month',
  ],
  [
    'type' => 'textarea',
    'name' => 'handling',
    'label' => 'Behandlung',
    'cols' => 80,
    'rows' => 6,
  ],
  [
    'type' => 'textarea',
    'name' => 'comments',
    'label' => 'Kommentar',
    'cols' => 80,
    'rows' => 6,
  ],
  [
    'type' => 'text',
    'name' => 'bottle',
    'label' => 'Vermehrungsgefäss',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'place',
    'label' => 'Vermehrungsstandort',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'select',
    'name' => 'success_id',
    'label' => 'Erfolg',
    'optionsql' => 'SELECT id, text FROM percent',
  ],
];

db_add2('form1', $table, $fields, '');
db_mod2('form1', $table, $fields);
db_del('form1', $table);
db_back('form1', 'mainpage.php');

page_begin();
page_title('Vermehrung');
db_form2('form1', $table, $fields);

page_end();
?>