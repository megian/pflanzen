<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Herkunft", "originform.php"),
  array("_Links", $_SERVER['PHP_SELF'])
);

include('default.inc.php');

$table = 'originlink';

$fields = [
  ...$fields_defaults,
  [
    'type' => 'add',
    'name' => 'origin_id',
    'value' => status_get('origin_id'),
  ],
  [
    'type' => 'text',
    'name' => 'name',
    'label' => 'Name',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'link',
    'label' => 'Link',
    'size' => 50,
    'maxlength' => 150,
  ],
];

db_add('form1', $table, $fields, '');
db_mod('form1', $table, $fields);
db_del('form1', $table);
db_back('form1', lastpage_get());

page_begin();
page_title('Herkunft Links');
db_form('form1', $table, $fields);

page_end();
?>
