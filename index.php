<?php

$toolbarentries = array(
  array("H_aupstamm", $_SERVER['PHP_SELF'])
);

include("default.inc.php");
page_begin();

$active = status_get("active");

if($active=="")
  $active = 1;

$menuentries = array(
  array("Haut_pstamm", "index.php?active=1", $active==1),
  array("_Familien", "index.php?active=2", $active==2),
  array("_Kategorien", "index.php?active=3", $active==3),
  array("_Links", "index.php?active=4", $active==4)
);

menu($menuentries);
echo '<div class="groupbox">';

if($active==1)
{
  
$menuentries = array(
  array("_Hinzufügen", "mainform.php?status=0")
);

  menu($menuentries);

  if(isset($_REQUEST['search']))
  {
    status_set_key("method", "1");
  }
  else if(isset($_REQUEST['o']))
  {
    status_set_key("method", "2");
  }

  $search = status_get('search');
  $o = status_get("o") ?? 0;
  $method = status_get("method");
  
  $limit = 50;
  $table = "main";
  if(isset($offset))
    $o = 0;
	
  echo "<p><form><input name=\"search\" value=\"$search\" /> <input type=\"submit\" value=\"Suchen\" /></form></p>";
    
  echo "<div class=\"menu-abc\">";
  
  $alpha = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
  
  for($i=0;$i<=25;$i++)
  {
    $tmp1 = status_get("o");
    $tmp2 = $alpha[$i]==$tmp1?"style=\"background-color:#fcffbb;\"":"";
    echo "<a href=\"index.php?o=".$alpha[$i]."\"".$tmp2.">".$alpha[$i]."</a> ";
  }
  echo "</div>";

$sqltablecolumns = array(
  array("ID", "3%"),
  array("", "3%"),
  array("Gattung"),
  array("Art"),
  array("Sorte"),
  array("Familie"),
  array("Kategorie")
);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onClick=\"location.href='mainpage.php?main_id=$data[0]'\"><td><a href=\"mainpage.php?main_id=$data[0]\">$data[0]</a></td><td><a href=\"mainform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td>$data[4]</td><td>$data[5]</td></tr>";
  }
  
  $ncount=50;
  if(status_get("form1o")=="")
    status_set("form1o", "0");
  
  if($method=="1")
  {
    $sql = "SELECT $table.id, $table.genus, $table.species, $table.variety, b.text, a.text FROM $table LEFT JOIN categorie AS a ON a.id=$table.categorie_id LEFT JOIN familia AS b ON b.id=$table.familia_id WHERE $table.id = '$search' or genus like '%$search%' or species like '%$search%' or variety like '%$search%' ORDER BY $table.genus";
    db_limit_navigation("form1", $sql, $ncount);
    sqltable_begin($sqltablecolumns);
    db_sql_multi($sql.db_limit("form1", $ncount), 'show');
	sqltable_end();
  }
  
  if($method=="2")
  {
    $sql = "SELECT $table.id, $table.genus, $table.species, $table.variety, b.text, a.text FROM $table LEFT JOIN categorie AS a ON a.id=$table.categorie_id LEFT JOIN familia AS b ON b.id=$table.familia_id WHERE $table.genus like '".status_get("o")."%' ORDER BY $table.genus";
    db_limit_navigation("form1", $sql, $ncount);
	sqltable_begin($sqltablecolumns);
    db_sql_multi($sql.db_limit("form1", $ncount), 'show');
    sqltable_end();
  }
  }
  if($active==2)
  {
  
$menuentries = array(
  array("_Hinzufügen", "familiaform.php?status=0")
);

  menu($menuentries);
  
  echo "<div class=\"menu-abc\">";
  
  $alpha = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
  
  for($i=0;$i<=25;$i++)
  {
    $tmp1 = status_get("o");
    $tmp2 = $alpha[$i]==$tmp1?"style=\"background-color:#fcffbb;\"":"";
    echo "<a href=\"index.php?o=".$alpha[$i]."\"".$tmp2.">".$alpha[$i]."</a> ";
  }
  echo "</div>";

  
$sqltablecolumns = array(
  array("", "3%"),
  array("Familien")
);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onClick=\"location.href='familiaform.php?id=$data[0]'\"><td><a href=\"familiaform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td></tr>";
  }
  
  $table = "familia";
  $sql = "SELECT $table.id, $table.text FROM $table WHERE $table.text != '' AND text like '".status_get("o")."%' ORDER BY text";
  sqltable_begin($sqltablecolumns);
  db_sql_multi($sql, 'show');
  sqltable_end();
      
  }
  if($active==3)
  {
  
$menuentries = array(
  array("_Hinzufügen", "categorieform.php?status=0")
);

  menu($menuentries);
  
$sqltablecolumns = array(
  array("", "3%"),
  array("Kategorie")
);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onClick=\"location.href='categorieform.php?id=$data[0]'\"><td><a href=\"categorieform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td></tr>";
  }
  
  $table = "categorie";
  $sql = "SELECT $table.id, $table.text FROM $table WHERE $table.text != ''";
  sqltable_begin($sqltablecolumns);
  db_sql_multi($sql, 'show');
  sqltable_end();
      
  }
  
  if($active==4)
  {
  
$menuentries = array(
  array("_Hinzufügen", "linkform.php?status=0")
);

  menu($menuentries);
  
$sqltablecolumns = array(
  array("", "3%"),
  array("Name", "25%"),
  array("Link")
);

  function show2($data)
  {
    global $template_img_edit;
    echo "<tr onClick=\"location.href='linkform.php?id=$data[0]'\"><td><a href=\"linkform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td><a href=\"$data[2]\">$data[2]</a></td></tr>";    
  }

  function show($data)
  {
    global $sqltablecolumns ;
    echo "<h1>$data[1]</h1>";
    sqltable_begin($sqltablecolumns);
    $table = "link";
    $sql = "SELECT $table.id, $table.name, $table.link FROM $table WHERE $table.categorie_id = $data[0]";
    db_sql_multi($sql, 'show2');
    sqltable_end();   
  }
  
  echo "<p style=\"margin-top:30px;\"></p>";
  
  $table = "linkcat";
  $sql = "SELECT $table.id, $table.text FROM $table WHERE $table.text != ''";
  db_sql_multi($sql, 'show');
      
  }
  
  echo '</div>';
  page_end();
?>