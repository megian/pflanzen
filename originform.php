<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Herkunft", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = 'origin';

$fields = [
  [
    'type' => 'seq',
    'name' => 'seq',
  ],
  [
    'type' => 'modtime',
    'name' => 'modtime',
  ],
  [
    'type' => 'add',
    'name' => 'main_id',
    'value' => status_get("main_id"),
  ],
  [
    'type' => 'text',
    'name' => 'origin',
    'label' => 'Herkunft',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'discoverername',
    'label' => 'Entdecker',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'discovererdate',
    'label' => 'Datum',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'textarea',
    'name' => 'origininformation',
    'label' => 'Herkunft Informationen',
    'cols' => 60,
    'rows' => 10,
  ],
];

db_add2('form1', $table, $fields, '');
db_mod2('form1', $table, $fields);
db_del('form1', $table);
db_back('form1', 'mainpage.php');

page_begin();
page_title('Herkunft');
db_form2('form1', $table, $fields);

if(status_get("id")!="")
{

$menuentries = array(
  array("_Link hinzufügen", "originlinkform.php?status=0&origin_id=".status_get("id"))
);

  menu($menuentries);
  
$sqltablecolumns = array(
  array("", "5%"),
  array("Name"),
  array("Link")
);
  sqltable_begin($sqltablecolumns);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onClick=\"location.href='originlinkform.php?id=$data[0]'\"><td><a href=\"originlinkform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td><a href=\"$data[2]\">$data[2]</a></td></tr>";
  }

  db_sql_multi("SELECT id, name, link FROM originlink WHERE origin_id=".status_get("id"), 'show');

  sqltable_end();
  
?>

</div>
    
<?php }

page_end();
?>