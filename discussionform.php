<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Diskussion", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = "discussion";

$fields = [
  [
    'type' => 'seq',
    'name' => 'seq',
  ],
  [
    'type' => 'modtime',
    'name' => 'modtime',
  ],
  [
    'type' => 'instime',
    'name' => 'inserttime',
  ],
  [
    'type' => 'add',
    'name' => 'account_id',
    'value' => $_SESSION['userid'],
  ],
  [
    'type' => 'add',
    'name' => 'main_id',
    'value' => status_get("main_id"),
  ],
  [
    'type' => 'text',
    'name' => 'subject',
    'label' => 'Betreff',
    'size' => 100,
    'maxlength' => 100,
  ],
  [
    'type' => 'textarea',
    'name' => 'text',
    'cols' => 85,
    'rows' => 6,
  ],
];


db_add2("form1", $table, $fields, "");
db_mod2("form1", $table, $fields);
db_del("form1", $table);
db_back("form1", "mainpage.php");

page_begin();
page_title("Diskussion");
db_form2("form1", $table, $fields);

page_end();
?>