<?php

require('config.inc.php');
require('lib/session.inc.php');
require('lib/function.inc.php');
require('lib/design.inc.php');
require('lib/form.inc.php');
require('lib/form2.inc.php');

// Initialize

session_start();

if(isset($_REQUEST['status']))
  if($_REQUEST['status']=='0')
    status_reset();
status_set();
lastpage_set();
db_open();

if($config_login==true)
  session();


// Fields default

$fields_defaults = [
  [
    'type' => 'seq',
    'name' => 'seq',
  ],
  [
    'type' => 'modtime',
    'name' => 'modtime',
  ],
];

$fields_defaults_main = [
  ...$fields_defaults,
  [
    'type' => 'add',
    'name' => 'main_id',
    'value' => status_get("main_id"),
  ],
];

?>