<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Links", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = 'mainlink';

$fields = [
  ...$fields_defaults_main,
  [
    'type' => 'text',
    'name' => 'name',
    'label' => 'Name',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'link',
    'label' => 'Link',
    'size' => 50,
    'maxlength' => 150,
  ],
];

db_add2("form1", $table, $fields, "");
db_mod2("form1", $table, $fields);
db_del("form1", $table);
db_back("form1", lastpage_get());

page_begin();
page_title("Links");
db_form2("form1", $table, $fields);

page_end();
?>