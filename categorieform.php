<?php

$toolbarentries = [
  [
    'H_aupstamm',
    'index.php',
  ],
  [
    '_Kategorien',
    $_SERVER['PHP_SELF'],
  ],
];

require("default.inc.php");

$table = 'categorie';

$fields = [
  [
    'type' => 'seq',
    'name' => 'seq',
  ],
  [
    'type' => 'modtime',
    'name' => 'modtime',
  ],
  [
    'type' => 'text',
    'label' => 'Kategorie',
    'name' => 'text',
    'size' => 50,
    'maxlength' => 50,
  ],
];

db_add2("form1", $table, $fields, "");
db_mod2("form1", $table, $fields);
db_del("form1", $table);
db_back("form1", lastpage_get());

page_begin();
page_title("Kategorie");

db_form2("form1", $table, $fields);

page_end();
?>