<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Links", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = "link";

$fields = [
  ...$fields_defaults,
  [
    'type' => 'select',
    'label' => 'Kategorie',
    'name' => 'categorie_id',
    'optionsql' => 'SELECT id, text FROM linkcat',
  ],
  [
    'type' => 'text',
    'label' => 'Name',
    'name' => 'name',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'label' => 'Link',
    'name' => 'link',
    'size' => 50,
    'maxlength' => 150,
  ],
];

db_add2("form1", $table, $fields, "");
db_mod2("form1", $table, $fields);
db_del("form1", $table);
db_back("form1", lastpage_get());

page_begin();
page_title("Links");
db_form2("form1", $table, $fields);

page_end();
?>