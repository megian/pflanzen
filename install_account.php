<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Accounts", $_SERVER['PHP_SELF'])
);

include("default.inc.php");

page_begin();

$table = "account";

  page_title("Accounts");

  menu(array(array("_Hinzufügen", "install_accountform.php?status=0")));

  $sqltablecolumns = array(
  array("", "5%"),
  array("Benutzer")
);
  sqltable_begin($sqltablecolumns);  

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='install_accountform.php?id=$data[0]'\"><td><a href=\"install_accountform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td></tr>";
  }
  
  db_sql_multi("SELECT $table.id, $table.displayname FROM $table", 'show');

  sqltable_end();
  echo '</div>';

page_end();
?>