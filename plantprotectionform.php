<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Pflanzenschutz", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = 'plantprotection';

$fields = [
  ...$fields_defaults_main,
  [
    'type' => 'text',
    'name' => 'diseases',
    'label' => 'Krankheit',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'select',
    'label' => 'Bekämpfung',
    'name' => 'combat_id',
    'optionsql' => 'SELECT id, text FROM combat',
  ],
  [
    'type' => 'echo',
    'html' => '<p><b>Zeit</b></p>',
  ],
  [
    'type' => 'text',
    'name' => 'plantprotectionyear',
    'label' => 'Jahr',
    'size' => 5,
    'maxlength' => 5,
  ],
  [
    'type' => 'echo',
    'html' => '<p><b>Befallzeit</b><p>',
  ],
  [
    'type' => 'select',
    'label' => 'von',
    'name' => 'beginweek',
    'optionsql' => 'SELECT id, text FROM week',
  ],
  [
    'type' => 'select',
    'name' => 'beginmonth',
    'optionsql' => 'SELECT id, text FROM month',
  ],
  [
    'type' => 'echo',
    'html' => '<br>',
  ],
  [
    'type' => 'select',
    'label' => 'bis',
    'name' => 'endweek',
    'optionsql' => 'SELECT id, text FROM week',
  ],
  [
    'type' => 'select',
    'name' => 'endmonth',
    'optionsql' => 'SELECT id, text FROM month',
  ],
  [
    'type' => 'echo',
    'html' => '<br>',
  ],
  [
    'type' => 'echo',
    'html' => '<p><b>Dosis</b></p>',
  ],
  [
    'type' => 'text',
    'name' => 'applicationmixes',
    'label' => 'Menge [g\ccm]',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'applicationtype',
    'label' => 'Produkt',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'applicationthinnes',
    'label' => 'Spritzmenge',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'echo',
    'html' => '<b>Weiteres</b><br>',
  ],
  [
    'type' => 'text',
    'name' => 'doseresponse',
    'label' => 'Wirkstoff',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'productname',
    'label' => 'Produktnamen',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'textarea',
    'name' => 'properties',
    'label' => 'Wirkungsart',
    'cols' => 50,
    'rows' => 10,
  ],
  [
    'type' => 'textarea',
    'name' => 'comments',
    'label' => 'Kommentar',
    'cols' => 50,
    'rows' => 10,
  ],
  [
    'type' => 'select',
    'name' => 'success_id',
    'label' => 'Erfolg',
    'optionsql' => 'SELECT id, text FROM percent',
  ],
];

db_add2("form1", $table, $fields, "");
db_mod2("form1", $table, $fields);
db_del("form1", $table);
db_back("form1", "mainpage.php");

page_begin();
page_title("Krankheiten");
db_form2("form1", $table, $fields);

page_end();
?>