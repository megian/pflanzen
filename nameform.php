<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Namen", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = 'name';

$fields = [
  ...$fields_defaults_main,
  [
    'type' => 'select',
    'label' => 'Synonym',
    'name' => 'cat_id',
    'optionsql' => 'SELECT id, text FROM namecat',
  ],
  [
    'type' => 'text',
    'name' => 'name',
    'label' => 'Name',
    'size' => 50,
    'maxlength' => 50,
  ],
];


db_add2('form1', $table, $fields, '');
db_mod2('form1', $table, $fields);
db_del('form1', $table);
db_back('form1', 'mainpage.php');

page_begin();
page_title('Synonyme und Ländernamen');
db_form2('form1', $table, $fields);

page_end();
?>