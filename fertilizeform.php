<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Dünger", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = "fertilize";

$fields = [
  ...$fields_defaults_main,
  [
    'type' => 'text',
    'name' => 'fertilizename',
    'label' => 'Name',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'select',
    'label' => 'Typ',
    'name' => 'fertilizetype_id',
    'optionsql' => 'SELECT id, text FROM fertilizetype',
  ],
  [
    'type' => 'text',
    'name' => 'fertilizemixes',
    'label' => 'Menge [g\ccm]',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'select',
    'label' => 'Art',
    'name' => 'fertilizesort_id',
    'optionsql' => 'SELECT id, text FROM fertilizesort',
  ],
];

db_add2("form1", $table, $fields, "");
db_mod2("form1", $table, $fields);
db_del("form1", $table);
db_back("form1", "mainpage.php");

page_begin();
page_title("Dünger");
db_form2("form1", $table, $fields);

page_end();
?>