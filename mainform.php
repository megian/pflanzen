<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = "main";

$fields = [
  [
    'type' => 'seq',
    'name' => 'seq',
  ],
  [
    'type' => 'modtime',
    'name' => 'modtime',
  ],
  [
    'type' => 'text',
    'name' => 'genus',
    'label' => 'Gattung',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'species',
    'label' => 'Art',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'text',
    'name' => 'variety',
    'label' => 'Sorte',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'select',
    'label' => 'Familie',
    'name' => 'familia_id',
    'optionsql' => 'SELECT id, text FROM familia ORDER BY text',
  ],
  [
    'type' => 'select',
    'label' => 'Kategorie',
    'name' => 'categorie_id',
    'optionsql' => 'SELECT id, text FROM categorie ORDER BY text',
  ],
];

db_add2("form1", $table, $fields, "");
db_mod2("form1", $table, $fields);
//db_del("form1", $table);
db_back("form1", lastpage_get());

page_begin();
page_title("Pflanzen");
db_form2("form1", $table, $fields);

page_end();
?>