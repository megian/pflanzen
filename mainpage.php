<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", $_SERVER['PHP_SELF'])
);

require('default.inc.php');
page_begin();

if(status_get("active")!="")
  $active = status_get("active");
else
  $active = 1;

$menuentries = array(
  array("Pflanzennamen b_earbeiten", "mainform.php?id=".status_get("main_id"))
);

  menu($menuentries);

  $table = "main";  
  $data = db_sql("SELECT $table.id, $table.genus, $table.species, $table.variety, b.text, a.text FROM $table LEFT JOIN categorie AS a ON a.id=$table.categorie_id LEFT JOIN familia AS b ON b.id=$table.familia_id WHERE $table.id = ".status_get("main_id"), false);
  
echo "<table style=\"width:100%\">";
echo "<tr class=\"mainpage-caption\">";
  echo "<td>ID</td>";
  echo "<td>Familie</td>";
  echo "<td>Kategorie</td>";
echo "</tr>";
echo "<tr class=\"mainpage-text\">";
  echo "<td>$data[0]</td>";
  echo "<td>$data[4]</td>";
  echo "<td>$data[5]</td>";
echo "</tr>";
echo "<tr class=\"mainpage-caption\">";
  echo "<td>Gattung</td>";
  echo "<td>Art</td>";
  echo "<td>Sorte</td>";
echo "</tr>";
echo "<tr class=\"mainpage-text\">";
  echo "<td>$data[1]</td>";
  echo "<td>$data[2]</td>";
  echo "<td>$data[3]</td>";
echo "</tr>";
echo "</table>\n";

$menuentries = array(
  array("S_ynonyme &amp; Ländernamen", "mainpage.php?active=1", $active==1),
  array("B_ilder", "mainpage.php?active=2", $active==2),
  array("H_erkunft", "mainpage.php?active=3", $active==3),
  array("_Beschrieb", "mainpage.php?active=4", $active==4),
  array("B_oden", "mainpage.php?active=5", $active==5),
  array("S_tandort", "mainpage.php?active=6", $active==6),
  array("Pf_lanzenschutz", "mainpage.php?active=7", $active==7),
  array("_Vermehrung", "mainpage.php?active=8", $active==8),
  array("Li_nks", "mainpage.php?active=9", $active==9),
  array("_Diskussion", "mainpage.php?active=10", $active==10)
);

  menu($menuentries);

/////////////////////////////////////
// Synonyme und Ländernamen

if($active==1)
{
  echo "<div class=\"groupbox\">";

  menu(array(array("_Hinzufügen", "nameform.php?status=0&main_id=".status_get("main_id"))));

  $sqltablecolumns = array(
  array("", "5%"),
  array("Kategorie"),
  array("Name")
);
  sqltable_begin($sqltablecolumns);  

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='nameform.php?id=$data[0]'\"><td><a href=\"nameform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td><a href=\"nameform.php?id=$data[0]\">$data[2]</a></td></tr>";
  }

  db_sql_multi("SELECT name.id, namecat.text, name.name FROM name LEFT JOIN namecat ON namecat.id = name.cat_id WHERE main_id=".status_get("main_id"), 'show');

  sqltable_end();
  echo "</div>";
}

/////////////////////////////////////
// BILDER

else if($active==2)
{
  echo "<div class=\"groupbox\">";

  $imgaction = status_get("imgaction");

  if(!isset($imgaction))
    $imgaction = 1;

$menuentries = array(
  array("_Tabelle", "mainpage.php?imgaction=1", $imgaction==1),
  array("_Galerie", "mainpage.php?imgaction=2", $imgaction==2)
);
  menu($menuentries);
  echo "<div class=\"groupbox\">";

  menu(array(array("_Hinzufügen", "imgform.php?status=0&main_id=".status_get("main_id"))));

if($imgaction==1)
{
  $sqltablecolumns = array(
  array("", "5%"),
  array("Kategorie", "10%"),
  array("Beschreibung"),
  array("Bild", "10%")
);
  sqltable_begin($sqltablecolumns);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='imgform.php?id=$data[0]'\"><td><a href=\"imgform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td>$data[2]</td><td><a href=\"imgpage.php?id=$data[0]\"><img class=\"thumbnail\" src=\"img/tmb/$data[0].jpg\" alt=\"\" /></a></td></tr>";
  }
  
  db_sql_multi("SELECT img.id, imgcat.text, img.text FROM img LEFT JOIN imgcat ON (imgcat.id=img.categorie_id) WHERE img.main_id=".status_get("main_id"), 'show');
}
else
{
  $i = 0;
  $sqltablecolumns = array(
  array("-", "33%"),
  array("-", "33%"),
  array("-", "33%")
);
  sqltable_begin($sqltablecolumns);    

  function show($data)
  {
    global $i;
	
	if($i==0)
	  echo "<tr>";
    
    echo "<td><a href=\"imgpage.php?id=$data[0]\"><img class=\"thumbnail\" src=\"img/tmb/$data[0].jpg\" alt=\"\"/></a></td>";
    
	if($i==2)
    {
      $i=0;
      echo "</tr>";
    }
	else
      $i++;
  }
  
  db_sql_multi("SELECT img.id FROM img LEFT JOIN imgcat ON (imgcat.id=img.categorie_id) WHERE img.main_id=".status_get("main_id"), 'show');

  if($i!=0 && $i<2)
    echo "</tr>";  
}
  sqltable_end();
  echo "</div>";
  echo "</div>";
} 

/////////////////////////////////////
// Herkunft

else if($active==3)
{
  echo "<div class=\"groupbox\">";

  menu(array(array("_Hinzufügen", "originform.php?status=0&main_id=".status_get("main_id"))));

  $sqltablecolumns = array(
  array("", "5%"),
  array("Herkunft"),
  array("Entdecker"),
  array("Datum")
);
  sqltable_begin($sqltablecolumns);
  
  function show($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='originform.php?id=$data[0]'\"><td><a href=\"originform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\"></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td></tr>";
  }

  db_sql_multi("SELECT origin.id, origin.origin, origin.discoverername, origin.discovererdate FROM origin WHERE origin.main_id=".status_get("main_id"), 'show');

  sqltable_end();
  echo "</div>";
} 

/////////////////////////////////////
// Beschreibung

else if($active==4)
{
  echo "<div class=\"groupbox\">";  

$descaction = status_get("descaction");

if(!isset($descaction))
  $descaction = 1;

$menuentries = array(
  array("Allge_meiner Beschrieb", "mainpage.php?descaction=1", $descaction==1),
  array("Blüten Besch_rieb", "mainpage.php?descaction=2", $descaction==2),
  array("_Frucht Beschrieb", "mainpage.php?descaction=3", $descaction==3),
  array("_Giftigkeit für Menschen", "mainpage.php?descaction=4", $descaction==4)
);

  menu($menuentries);

$id = status_get("main_id");

if($descaction==1) {

$table = "description";

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("select", "Wuchsform", "habit_id", "SELECT id, text FROM habit"),
  array("text", "Höhe [cm]", "height", "", 5, 10),
  array("text", "Breite [cm]", "width", "", 5, 10),
  array("select", "Wurzelform", "rootsystem_id", "SELECT id, text FROM rootsystem"),
  array("select", "Wuchstärke", "growthstrength_id", "SELECT id, text FROM strength"),
  array("text", "Blattfarbe", "leafcolor", "", 50, 50),
  array("text", "Herbstfärbung", "fallfoliagecolor", "", 50, 50),
  array("text", "Blattform", "leafshape", "", 50, 50),
  array("text", "Blattstellung", "leafposition", "", 50, 50),
  array("select", "Belaubung", "foliation_id", "SELECT id, text FROM foliation")
);

db_add("form1", $table, $felder, "");
db_mod("form1", $table, $felder);
db_del("form1", $table);
db_form("form1", $table, $felder);

} else if($descaction==2) {

$table = "description";

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("text", "Blütenfarbe", "bloomcolor", "", 50, 50),
  array("say", "<b>Blütezeit</b>"),
  array("select", "von", "bloombeginweek", "SELECT id, text FROM week"),
  array("select", "", "bloombeginmonth", "SELECT id, text FROM month"),
  array("say", "<br />"),
  array("select", "bis", "bloomendweek", "SELECT id, text FROM week"),
  array("select", "", "bloomendmonth", "SELECT id, text FROM month"),
  array("say", "<br />"),
  array("select", "Befruchtung", "bloomfretilization_id", "SELECT id, text FROM bloomfretilization"),
  array("text", "Befruchtersorte", "bloomfretilizationtype", "", 50, 50),
  array("text", "Blütenform", "bloomform", "", 50, 50),
  array("select", "Duftend", "bloomfragrant_id", "SELECT id, text FROM strength"),
  array("text", "Geschmack", "bloomtaste", "", 50, 50)
);

db_add("form2", $table, $felder, "");
db_mod("form2", $table, $felder);
db_del("form2", $table);
db_form("form2", $table, $felder);

} else if($descaction==3) {

$table = "description";

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("text", "Frucht Form", "fruitform", "", 50, 50),
  array("say", "<br /><b>Essbar</b>"),
  array("checkbox", "&nbsp;&nbsp;Samen", "fruiteatableseed", ""),
  array("checkbox", "&nbsp;&nbsp;Fruchtfleisch", "fruiteatablemeat", ""),
  array("checkbox", "&nbsp;&nbsp;Haut", "fruiteatableskin", ""),
  array("say", "<br />"),
  array("checkbox", "Lagerbar", "fruitstockable", ""),
  array("textarea", "Fruchtbeschreibung", "fruittext", "", 80, 10),
  array("text", "Fruchtfarbe", "fruitcolor", "", 50, 50),
  array("text", "Fruchtgrösse", "fruitsize", "", 10, 20),
  array("say", "<b>Reifezeit</b>"),
  array("select", "von", "fruitbeginweek", "SELECT id, text FROM week"),
  array("select", "", "fruitbeginmonth", "SELECT id, text FROM month"), 
  array("say", "<br />"),
  array("select", "bis", "fruitendweek", "SELECT id, text FROM week"),
  array("select", "", "fruitendmonth", "SELECT id, text FROM month"),  
  array("say", "<br />"),
  array("text", "Geschmack", "fruittaste", "", 50, 50),
  array("checkbox", "Bienennährpflanze", "fruitbeefeedplant", "")
);

db_add("form3", $table, $felder, "");
db_mod("form3", $table, $felder);
db_del("form3", $table);
db_form("form3", $table, $felder);
  
} else {

$table = "description";

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("checkbox", "Blätter", "fruittoxleaves", ""),
  array("checkbox", "Früchte", "fruittoxfruits", ""),
  array("checkbox", "Holz", "fruittoxwood", ""),
  array("checkbox", "Wurzel", "fruittoxroot", "")
);

db_add("form4", $table, $felder, "");
db_mod("form4", $table, $felder);
db_del("form4", $table);
db_form("form4", $table, $felder);
  
}
  echo "</div>";
} 

/////////////////////////////////////
// Boden

else if($active==5)
{
  echo "<div class=\"groupbox\">";

$id = status_get("main_id");

$table = "ground";

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("text", "Bodenart", "soiltype", "", 20, 20),
  array("text", "PH-Wert", "soilph", "", 10, 10),
  array("text", "Wasserbedarf", "waterdemand", "", 20, 20),
  array("text", "Bodenempfindlichkeit", "soilsensitivity", "", 20, 20)
);

db_add("form1", $table, $felder, "");
db_mod("form1", $table, $felder);
db_del("form1", $table);
db_form("form1", $table, $felder);

  // Dünger

 page_title("Dünger");

  menu(array(array("_Hinzufügen", "fertilizeform.php?status=0&main_id=".status_get("main_id"))));

  $table = "fertilize";
  
$sqltablecolumns = array(
  array("", "5%"),
  array("Name"),
  array("Typ"),
  array("Menge [g\ccm]"),
  array("Art")
);
  sqltable_begin($sqltablecolumns);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='fertilizeform.php?id=$data[0]'\"><td><a href=\"fertilizeform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td><td>$data[4]</td></tr>";
  }

  db_sql_multi("SELECT $table.id, $table.fertilizename, fertilizetype.text, $table.fertilizemixes, fertilizesort.text FROM $table LEFT JOIN fertilizetype ON $table.fertilizetype_id = fertilizetype.id LEFT JOIN fertilizesort ON $table.fertilizesort_id = fertilizesort.id WHERE $table.main_id=".status_get("main_id"), 'show');

  sqltable_end();
  
  
  // Substrate

  page_title("Substrate");

  menu(array(array("_Hinzufügen", "substrateform.php?status=0&main_id=".status_get("main_id"))));

  $table = "substrate";
  
$sqltablecolumns = array(
  array("", "5%"),
  array("Substrate")
);
  sqltable_begin($sqltablecolumns);

  function show2($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='substrateform.php?id=$data[0]'\"><td><a href=\"substrateform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td></tr>";
  }

  db_sql_multi("SELECT $table.id, $table.name FROM $table WHERE $table.main_id=".status_get("main_id"), 'show2');

  sqltable_end();
  echo "</div>";
} 

/////////////////////////////////////
// Standort

else if($active==6)
{
  echo "<div class=\"groupbox\">";

$id = status_get("main_id");

$table = "place";

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("select", "Standort", "place_id", "SELECT id, text FROM placetext"),
  array("text", "Verwendung", "application", "", 20, 50),
  array("text", "Zugehörige Pflanzen", "relatedplants", "", 20, 50),
  array("text", "Lebensbereich", "sphere", "", 20, 50),
  array("text", "Winterhärte zone", "winterhardeningzone", "", 10, 10),
  array("text", "Minimum Temperatur", "mintemperature", "", 20, 20)
);

db_add("form1", $table, $felder, "");
db_mod("form1", $table, $felder);
db_del("form1", $table);
db_form("form1", $table, $felder);

  echo "</div>";
} 

/////////////////////////////////////
// Pflanzenschutz

else if($active==7)
{
  echo "<div class=\"groupbox\">";

$menuentries = array(
  array("_Hinzufügen", "plantprotectionform.php?status=0&status=0&main_id=".status_get("main_id"))
);

  menu($menuentries);

  $table = "plantprotection";
  
$sqltablecolumns = array(
  array("", "5%"),
  array("Krankheit"),
  array("Art"),
  array("Von"),
  array("Bis"),
  array("Wirkstoff")
);
  sqltable_begin($sqltablecolumns);

  function show($data)
  {
    global $template_img_edit; 
    echo "<tr onclick=\"location.href='plantprotectionform.php?id=$data[0]'\"><td><a href=\"plantprotectionform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3] - $data[4]</td><td>$data[5] - $data[6]</td><td>$data[7]</td></tr>";
  }

  db_sql_multi("SELECT $table.id, $table.diseases, art.text, w1.text, m1.text, w2.text, m2.text, $table.doseresponse FROM $table LEFT JOIN combat AS art ON (art.id=$table.combat_id) LEFT JOIN month AS m1 ON (m1.id=$table.beginmonth) LEFT JOIN month AS m2 ON (m2.id=$table.endmonth) LEFT JOIN week AS w1 ON (w1.id=$table.beginweek) LEFT JOIN week AS w2 ON (w2.id=$table.endweek) WHERE $table.main_id=".status_get("main_id"), 'show');

  sqltable_end();
  echo "</div>";
} 

/////////////////////////////////////
// Vermehrung

else if($active==8)
{
  echo "<div class=\"groupbox\">";

  menu(array(array("_Hinzufügen", "propagationform.php?status=0&main_id=".status_get("main_id"))));

  $table = "propagation";
  
  $sqltablecolumns = array(
  array("", "5%"),
  array("Vermehrung"),
  array("Von"),
  array("Bis"),
  array("Behandlung"),
  array("Erfolg"),
  array("Kommentare")
);
  sqltable_begin($sqltablecolumns);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='propagationform.php?id=$data[0]'\"><td><a href=\"propagationform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td>$data[2] - $data[3]</td><td>$data[4] - $data[5]</td><td>$data[6]</td><td>$data[7]</td><td>$data[8]</td></tr>";
  }

  db_sql_multi("SELECT $table.id, $table.propagation, w1.text, m1.text, w2.text, m2.text, $table.handling, s1.text, $table.comments FROM $table LEFT JOIN percent AS s1 ON s1.id = $table.success_id LEFT JOIN month AS m1 ON (m1.id=$table.beginmonth) LEFT JOIN month AS m2 ON (m2.id=$table.endmonth) LEFT JOIN week AS w1 ON (w1.id=$table.beginweek) LEFT JOIN week AS w2 ON (w2.id=$table.endweek) WHERE $table.main_id=".status_get("main_id"), 'show');

  sqltable_end();
  echo "</div>";
}

/////////////////////////////////////
// Links

else if($active==9)
{
  echo "<div class=\"groupbox\">";

$menuentries = array(
  array("_Link hinzufügen", "mainlinkform.php?status=0&main_id=".status_get("main_id"))
);

  menu($menuentries);
  
$sqltablecolumns = array(
  array("", "5%"),
  array("Name"),
  array("Link")
);
  sqltable_begin($sqltablecolumns);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onClick=\"location.href='mainlinkform.php?id=$data[0]'\"><td><a href=\"mainlinkform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td><a href=\"$data[2]\">$data[2]</a></td></tr>";
  }

  db_sql_multi("SELECT id, name, link FROM mainlink WHERE main_id=".status_get("main_id"), 'show');

  sqltable_end();
  
  echo "</div>";
}

/////////////////////////////////////
// Diskussion

else if($active==10)
{
  echo "<div class=\"groupbox\">";

$menuentries = array(
  array("_Eintrag hinzufügen", "discussionform.php?status=0&amp;main_id=".status_get("main_id"))
);

  menu($menuentries);
  
$sqltablecolumns = array(
  array("", "5%"),
  array("Zeit", "20%"),
  array("Benutzer", "20%"),
  array("Betreff")
);
  sqltable_begin($sqltablecolumns);

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onclick=\"location.href='discussionform.php?id=$data[0]'\"><td><a href=\"discussionform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[1]</td><td>$data[2]</td><td>$data[3]</td></tr>";
  }

  db_sql_multi("SELECT discussion.id, discussion.inserttime, account.displayname, discussion.subject FROM discussion LEFT JOIN account ON account.id=discussion.account_id WHERE main_id=".status_get("main_id"), 'show');

  sqltable_end();
  
  echo "</div>";
}

page_end();

?>