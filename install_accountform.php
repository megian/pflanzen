<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Accounts", "install_account.php"),
  array("_Account", $_SERVER['PHP_SELF'])
);

include("default.inc.php");

$table = "account";

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("text", "Anzeigename", "displayname", "Nachname, Vorname", 25, 25),
  array("text", "Benutzername", "username", "", 25, 25),
  array("password", "Passwort", "password", "", 25, 25),
  array("select", "Theme", "theme_id", "SELECT id, text FROM theme")
);

db_add("form1", $table, $felder, "");
db_mod("form1", $table, $felder);
db_del("form1", $table);
db_back("form1", "install_account.php");

page_begin();
page_title($table);
db_form("form1", $table, $felder);

page_end();
?>