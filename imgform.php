<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Bilder", $_SERVER['PHP_SELF'])
);

require("lib/tmb.inc.php");
require("default.inc.php");

$table = "img";

$fields = [
  ...$fields_defaults_main,
  [
    'type' => 'select',
    'label' => 'Kategorie',
    'name' => 'categorie_id',
    'optionsql' => 'SELECT id, text FROM imgcat',
  ],
  [
    'type' => 'textarea',
    'name' => 'text',
    'label' => 'Beschreibung',
    'cols' => 80,
    'rows' => 10,
  ],
  [
    'type' => 'idfile',
    'name' => 'ext',
    'label' => 'Datei',
    'path' => "img/",
  ],
];

db_add2("form1", $table, $fields, "");
db_add_tmb2("form1", $fields, "img/tmb/");
db_mod("form1", $table, $fields);
db_del_idfile("form1", $table, "ext", "img/");
db_del_idfile("form1", "", "jpg", "img/tmb/");
db_del("form1", $table);
db_back("form1", "mainpage.php");

page_begin();
page_title("Bilder");
db_form2("form1", $table, $fields);

page_end();
?>