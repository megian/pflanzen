<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Installation", $_SERVER['PHP_SELF'])
);

include("default.inc.php");

$filename = "install_tables.php";

if(status_get("active")!="")
  $active = status_get("active");
else
  $active = 0;

$menuentries = array(
  array("Blumen Befruchtung", $filename."?active=0", $active==0),
  array("Hauptstamm Kategorien", $filename."?active=1", $active==1),
  array("Krankheits Bekämpfung", $filename."?active=2", $active==2),
  array("Hautstamm Familien", $filename."?active=3", $active==3),
  array("Dünger Typ", $filename."?active=4", $active==4),
  array("Dünger Art", $filename."?active=5", $active==5),
  array("Belaubung", $filename."?active=6", $active==6),
  array("Wuchsform", $filename."?active=7", $active==7),
  array("Bilderkategorien", $filename."?active=8", $active==8),
  array("Monate", $filename."?active=9", $active==9),
  array("Names Kategorie", $filename."?active=10", $active==10),
  array("Prozent", $filename."?active=11", $active==11),
  array("Standorttext", $filename."?active=12", $active==12),
  array("Wurzelsystem", $filename."?active=13", $active==13),
  array("Stärke", $filename."?active=14", $active==14),
  array("Themes", $filename."?active=15", $active==15),
  array("Wochen", $filename."?active=16", $active==16),
  array("Link Kategorien", $filename."?active=17", $active==17)  
);

$tables = array(
  "bloomfretilization",
  "categorie",
  "combat",
  "familia",
  "fertilizesort",
  "fertilizetype",
  "foliation",
  "habit",
  "imgcat",
  "month",
  "namecat",
  "percent",
  "placetext",
  "rootsystem",
  "strength",
  "theme",
  "week",
  "linkcat"
);

page_begin();
menu($menuentries);

$table = $tables[$active];

echo "<div class=\"groupbox\">";

  page_title("Tabelle: ".$table);

  menu(array(array("_Hinzufügen", "install_tablesform.php?status=0&table_id=$active")));

  $sqltablecolumns = array(
  array("", "5%"),
  array("ID"),
  array("Text")
);
  sqltable_begin($sqltablecolumns);  

  function show($data)
  {
    global $template_img_edit;
    global $active;
    echo "<tr onclick=\"location.href='install_tablesform.php?id=$data[0]&table_id=$active'\"><td><a href=\"install_tablesform.php?id=$data[0]&table_id=$active\"><img src=\"$template_img_edit\" alt=\"Edit\" /></a></td><td>$data[0]</td><td>$data[1]</td></tr>";
  }
  
  db_sql_multi("SELECT $table.id, $table.text FROM $table", 'show');

  sqltable_end();
  echo "</div>";

page_end();
?>