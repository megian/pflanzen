<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Installation", "install_tables.php"),
  array("_Form", $_SERVER['PHP_SELF'])
);

$tables = array(
  "bloomfretilization",
  "categorie",
  "combat",
  "familia",
  "fertilizesort",
  "fertilizetype",
  "foliation",
  "habit",
  "imgcat",
  "month",
  "namecat",
  "percent",
  "placetext",
  "rootsystem",
  "strength",
  "theme",
  "week",
  "linkcat"
);

include("default.inc.php");

$table_id = status_get("table_id");
$table = $tables[$table_id];

$felder = array(
  array("seq", "", "seq"),
  array("modtime", "", "modtime"),
  array("text", "ID", "id", "", 10, 10),
  array("text", "Text", "text", "", 50, 50)
);

db_add("form1", $table, $felder, "");
db_mod("form1", $table, $felder);
db_del("form1", $table);
db_back("form1", "install_tables.php");

page_begin();
page_title($table);
db_form("form1", $table, $felder);

page_end();
?>