<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Substrate", "substrateform.php"),
  array("_Substrat Bestanteile", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = 'substrateparts';

$fields = [
  ...$fields_defaults,
  [
    'type' => 'add',
    'name' => 'substrate_id',
    'value' => status_get("substrate_id"),
  ],
  [
    'type' => 'text',
    'name' => 'text',
    'label' => 'Art',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'select',
    'name' => 'percent_id',
    'label' => 'Anteil',
    'optionsql' => 'SELECT id, text FROM percent',
  ],
];

db_add2('form1', $table, $fields, '');
db_mod2('form1', $table, $fields);
db_del('form1', $table);
db_back('form1', lastpage_get());

page_begin();
page_title('Bestanteil');
db_form2('form1', $table, $fields);

page_end();
?>