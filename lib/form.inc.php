<?php
//-----------------------------------------------------------------------------
// @library        form.inc.php
// @version        1.0
// @date           20.7.2003
// @update         18.01.2004
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
// @licence        GPL
//-----------------------------------------------------------------------------
// Liddle CMS - Form Designer
// Copyright (C) 2003-2005 Gabriel Mainberger <gabisoft@freesurf.ch>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//------------------------------------------------------------------------------
//
// History:
//
// 26.10.2003 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - First Publicated Version
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "smart" to db_form()
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "select" to db_form()
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "file" to db_form()
// 21.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Fix </text> to </textarea>
// 25.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "idfile" to db_form()
// 09.02.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - XHTML 1.1 compatible code
// 14.02.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "htmlarea" to db_form()
// 16.07.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "heckbox" to db_form()
// 17.10.2005 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - reimplement for better code style ;-)
// 27.11.2005 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add element add for insert for insert table only
// 29.01.2006 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Change to UTF-8
//
//------------------------------------------------------------------------------

// Filename: File Extention

function extractfileext($filename)
{
  $s = preg_split('\.', $filename);

  if(count($s)>1)
    return($s[count($s)-1]);

  return("");
}

// Feldtyp, Bezeichnung, DB-Name, default, grösse, maximal

function db_form($formname, $table, $fields)
{
  global $id;
  global $mysql_connect_handle;

  $field_type = "";
  $field_description = "";
  $field_name = "";
  $field_default = "";
  $field_var1 = "";
  $field_var2 = "";
  $field_var3 = "";

  $add_special_id = 0;

  if(!isset($id))
   $id = status_get("id");

  if(isset($_POST[$formname.'_del']))
    unset($id);

  echo '<form accept-charset="utf-8" method="post" enctype="multipart/form-data" action="'.htmlentities($_SERVER['PHP_SELF']).'">';

  if(isset($id))
  {
    $result = mysqli_query($mysql_connect_handle, "SELECT id FROM $table WHERE id = $id");
    $rows = mysqli_num_rows($result);
    mysqli_free_result($result);
    if($rows==1)
    {
      echo '<input type="hidden" name="'.$formname.'_mod" value="'.$id.'">';
      if(isset($id))
      {
        $sql = "SELECT ";
        $t = 0;
        for($i=0;$i<count($fields);$i++)
        {
          // Initalize
          $field_type = $fields[$i][0];
          $field_name = $fields[$i][2];

          if(($field_type=="text")
          || ($field_type=="textarea")
          || ($field_type=="password")
          || ($field_type=="select")
          || ($field_type=="file")
          || ($field_type=="idfile")
          || ($field_type=="htmlarea")
          || ($field_type=="checkbox")
          || ($field_type=="seq"))
          {
            if($t)
              $sql .= ', ';

            $sql .= $field_name;
            $t = 1;
          }
        }

        $sql .= " FROM $table WHERE id = $id";

        echo 'k'.$sql;

        $data = db_sql($sql, $mysql_connect_handle);
      }
    }
    else
    {
      echo "<input type=\"hidden\" name=\"".$formname."_add\" value=\"$id\" />\n";
      $add_special_id = 1;
    }
  }
  else
    echo "<input type=\"hidden\" name=\"".$formname."_add\" value=\"0\" />\n";

  // hidden fields
  $d = 0;
  for($i=0;$i<count($fields);$i++)
  {
    // Initalize
    $field_type = $fields[$i][0];
    $field_name = $fields[$i][2];
    $field_default = $fields[$i][3] ?? '';

    if($field_type=="hidden")
      echo "  <input type=\"hidden\" name=\"".$formname."_".$field_name."\" value=\"".$field_default."\" />\n";
    else if($field_type=="silent")
      echo "  <input type=\"hidden\" name=\"".$field_name."\" value=\"".$field_default."\" />\n";
    else if(($field_type=="add") && (!isset($id)))
      echo "  <input type=\"hidden\" name=\"".$formname."_".$field_name."\" value=\"".$field_default."\" />\n";
  }

  echo '<table>';

  for($i=0;$i<count($fields);$i++)
  {
    // Initalize
    $field_type = $fields[$i][0];
    $field_description = $fields[$i][1];
    $field_name = $fields[$i][2];
    $field_default = $fields[$i][3] ?? '';
    $field_var1 = $fields[$i][4] ?? '';
    $field_var2 = $fields[$i][5] ?? '';

    if(in_array($field_type, ['hidden', 'silent', 'add']))
      continue;

    if($field_type=="seq")
    {
      echo "<input type=\"hidden\" name=\"".$formname."_seq\" value=\"".(isset($id)?(($data[$d++] ?? 0)+1):"1")."\">\n";
      continue;
    }

    echo "<tr>\n";

    if($field_description!="say")
      echo "  <td>$field_description</td>\n";

    if(($field_type=="text") or ($field_type=="password"))
      echo "  <td><input type=\"$field_type\" name=\"".$formname."_".$field_name."\" maxsize=\"$field_var1\" size=\"$field_var2\" value=\"".(isset($id)&&$add_special_id==0?$data[$d++]:$field_default)."\" /></td>\n";
    else if($field_type=="textarea")
      echo "  <td><textarea name=\"".$formname."_".$field_name."\" cols=\"$field_var1\" rows=\"$field_var2\">".(isset($id)&&$add_special_id==0?$data[$d++]:$field_default)."</textarea></td>\n";
    else if($field_type=="htmlarea")
      echo "  <td><textarea id=\"ta\" name=\"ta\" cols=\"$field_var1\" rows=\"$field_var2\" style=\"width: 100%\">".(isset($id)&&$add_special_id==0?$data[$d++]:$field_default)."</textarea></td>\n";
    else if($field_type=="checkbox")
    {
      if($data[$d++]=='1')
        echo ' <td><input type="checkbox" name="'.$formname."_".$field_name.'" checked="checked"></td>';
      else
        echo ' <td><input type="checkbox" name="'.$formname."_".$field_name.'"></td>';
    }
    else if($field_type=="select")
    {
      $selectedid = $data[$d++] ?? 0;
      echo "  <td><select name=\"".$formname."_".$field_name."\" size=\"1\">";
      $result = mysqli_query($mysql_connect_handle, $field_default);
      while($row=mysqli_fetch_row($result))
      {
        if($row[0]==$selectedid)
          echo "<option selected=\"selected\" value=\"".$row[0]."\">".$row[1]."</option>";
        else
          echo "<option value=\"".$row[0]."\">".$row[1]."</option>";
      }
      mysqli_free_result($result);
      echo '</select></td>';
    }
    else if($field_type=="file")
    {
      if(isset($id))
        echo "  <td><a href=\"".$field_var1.$data[$d]."\">".$data[$d++]."</a></td>\n";
      else
      {
        echo "  <td><input type=\"file\" name=\"".$formname."_".$field_name."\"></td>\n";
        $d++;
      }
    }
    else if($field_type=="idfile")
    {
      if(isset($id))
      {
        if($data[$d]!="")
          $ext = ".".$data[$d++];
        echo "  <td><a href=\"".$field_var1.$id.$ext."\">".$id.$ext."</a></td>\n";
      }
      else
      {
        echo "  <td><input type=\"file\" name=\"".$formname."_".$field_name."\" size=\"80\" /></td>\n";
        $d++;
      }
    }
     else
      echo "  <td colspan=\"2\">$field_default</td>\n";

    echo "</tr>\n";
  }

  echo "  <tr><td></td><td>";

  if(isset($id) && $add_special_id==0)
  {
    echo "<input type=\"submit\" value=\"Ändern\" accesskey=\"s\" /></form>";
    echo "<form method=\"post\"><input type=\"hidden\" name=\"".$formname."_del\" value=\"$id\" /><input type=\"submit\" value=\"Löschen\" /></form>";
  }
  else
    echo "<input type=\"submit\" value=\"Einfügen\" accesskey=\"s\" /></form>";

  echo "</td></tr></table>\n";
}

function db_add($formname, $table, $fields, $text)
{
  global $id;
  global $mysql_connect_handle;

  if(isset($_POST[$formname.'_add']))
  {
    $sql = "INSERT INTO $table (";

    if($_POST[$formname.'_add']!="0") $sql .= "id, ";

    $t = 0;
    for($i=0;$i<count($fields);$i++)
    {
      if(($fields[$i][0]=="text") || ($fields[$i][0]=="textarea") || ($fields[$i][0]=="password") || ($fields[$i][0]=="hidden") ||
      ($fields[$i][0]=="select") || ($fields[$i][0]=="file") || ($fields[$i][0]=="idfile") || ($fields[$i][0]=="htmlarea") || ($fields[$i][0]=="checkbox") || ($fields[$i][0]=="seq") || ($fields[$i][0]=="add") || ($fields[$i][0]=="modtime") || ($fields[$i][0]=="instime"))
      {
        if($t)
          $sql .= ", ";

        $sql .= $fields[$i][2];
        $t = 1;
      }
    }

    $sql .= ") VALUES (";

    if($_POST[$formname.'_add']!="0") $sql .= $id.", ";

    $t = 0;
    for($i=0;$i<count($fields);$i++)
    {
      if(($fields[$i][0]=="text") || ($fields[$i][0]=="textarea") || ($fields[$i][0]=="password") || ($fields[$i][0]=="hidden") ||
      ($fields[$i][0]=="select") || ($fields[$i][0]=="file") || ($fields[$i][0]=="idfile") || ($fields[$i][0]=="htmlarea") || ($fields[$i][0]=="checkbox") || ($fields[$i][0]=="seq") || ($fields[$i][0]=="add") || ($fields[$i][0]=="modtime") || ($fields[$i][0]=="instime"))
      {
        if($t)
          $sql .= ", ";

        if($fields[$i][0]=="file")
          $sql .= "'".$_FILES[$formname."_".$fields[$i][2]]["name"]."'";
        else if($fields[$i][0]=="idfile")
          $sql .= "'".extractfileext($_FILES[$formname."_".$fields[$i][2]]["name"])."'";
        else if($fields[$i][0]=="htmlarea")
          $sql .= "'".$_POST[ta]."'";
        else if($fields[$i][0]=="checkbox")
        {
          if($_POST[$formname."_".$fields[$i][2]]=="on")
            $sql .= "'1'";
          else
            $sql .= "'0'";
        }
        else if($fields[$i][0]=="modtime")
          $sql .= "NOW()";
        else if($fields[$i][0]=="instime")
          $sql .= "NOW()";
        else
          $sql .= "'".$_POST[$formname."_".$fields[$i][2]]."'";
        $t = 1;
      }
    }

    $sql .= ")";

    //echo $sql;

    if(!@mysqli_query($mysql_connect_handle, $sql))
      errormsg("Konnte Daten nicht in die Datenbank eintragen!");

    if($text!="")
    {
      echo $text;
      footer();
      page_end();
      exit;
    }

    if(isset($_POST[$formname.'_add']))
      if($_POST[$formname.'_add']=="0")
        $id = mysqli_insert_id();

    for($i=0;$i<count($fields);$i++)
    {
      if($fields[$i][0]=="file")
        move_uploaded_file($_FILES[$formname."_".$fields[$i][2]]["tmp_name"], $fields[$i][4].$_FILES[$formname."_".$fields[$i][2]]["name"]);
      if($fields[$i][0]=="idfile")
      {
        $ext = extractfileext($_FILES[$formname."_".$fields[$i][2]]["name"]);
        if($ext != "")
          $ext = ".$ext";

        move_uploaded_file($_FILES[$formname."_".$fields[$i][2]]["tmp_name"], $fields[$i][4].$id.$ext);
      }
    }
  }
}

function db_add_tmb($formname, $fields, $dir)
{
  global $id;

  if(isset($_POST[$formname.'_add']))
  {
    for($i=0;$i<count($fields);$i++)
    {
      if($fields[$i][0]=="idfile")
      {
        $ext = extractfileext($_FILES[$formname."_".$fields[$i][2]]["name"]);
        if($ext=="jpg" || $ext=="png" || $ext=="gif")
        {
          //echo $fields[$i][4].$id.".".$ext;
          thumbjpg($fields[$i][4].$id.".".$ext, $dir.$id.".jpg", 150, 150, $quality=40);
        }
      }
    }
  }
}

function db_back($formname, $url)
{
  if(array_key_exists($formname.'_add', $_POST) || array_key_exists($formname.'_mod', $_POST) || array_key_exists($formname.'_del', $_POST))
  {
    header("Location: ".$url);
  }
}

function db_mod($formname, $table, $fields)
{
  global $id;
  global $mysql_connect_handle;

  if(isset($_POST[$formname.'_mod']))
  {
    $sql = "UPDATE $table SET ";

    $t = 0;
    for($i=0;$i<count($fields);$i++)
    {
      $field_type = $fields[$i][0];
      $valid_fields = [
        'text',
        'textarea',
        'password',
        'hidden',
        'select',
        'htmlarea',
        'checkbox',
        'seq',
        'modtime',
      ];

      if(in_array($field_type, $valid_fields))
      {
        if($t)
          $sql .= ", ";

        if($field_type=="htmlarea")
          $sql .= $fields[$i][2]." = '".$_POST['ta']."'";
        else if($field_type=="modtime")
          $sql .= $fields[$i][2]." = NOW()";
        else if($field_type=="checkbox")
        {
          $sql .= $fields[$i][2]." = ";
          if($_POST[$formname."_".$fields[$i][2]]=="on")
            $sql .= "'1'";
          else
            $sql .= "'0'";
        }
        else
          $sql .= $fields[$i][2]." = '".$_POST[$formname."_".$fields[$i][2]]."'";
        $t = 1;
      }
    }

    $sql .= " WHERE id = ".$_POST[$formname.'_mod'];

    //echo $sql;

    if(!mysqli_query($mysql_connect_handle, $sql))
      errormsg("Konnte Daten nicht ändern!");

    $id = $_POST[$formname.'_mod'];
  }
}

function db_del_idfile($formname, $table, $field, $path)
{
  global $mysql_connect_handle;
  global $id;

  if(array_key_exists($formname.'_del', $_POST))
  {
    if($table!="")
    {
      $sql = "SELECT $field FROM $table WHERE id = ".$_POST[$formname.'_del'];

      $data = db_sql($sql, $mysql_connect_handle);
      if($data[0]!="")
        $ext = ".".$data[0];
    }
    else
      $ext = ".".$field;

    unlink($path.$_POST[$formname.'_del'].$ext);
  }
}

function db_del_file($formname, $table, $field, $path)
{
  global $mysql_connect_handle;
  global $id;

  if(array_key_exists($formname.'_del', $_POST))
  {
    $sql = "SELECT $field FROM $table WHERE id = ".$_POST[$formname.'_del'];
    $data = db_sql($sql, $mysql_connect_handle);

    unlink($path.$data[0]);
  }
}

function db_del($formname, $table)
{
  global $mysql_connect_handle;
  global $id;

  if(array_key_exists($formname.'_del', $_POST))
  {
    $sql = "DELETE FROM $table WHERE id = ".$_POST[$formname.'_del'];

    if(!mysqli_query($mysql_connect_handle, $sql))
      errormsg("Konnte Daten nicht ändern!");
  }
}

?>