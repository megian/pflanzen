<?php
//-----------------------------------------------------------------------------
// @library        project.inc.php
// @version        1.0
// @date           2.6.2003
// @update         24.12.2005
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
// @licence        GPL
//-----------------------------------------------------------------------------
// Liddle CMS - Design
// Copyright (C) 2003-2005 Gabriel Mainberger <gabisoft@freesurf.ch>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// @function        page_begin()
// @paramter        HTML-Titel, Navigation-Titel
// @return        nichts
// @description        Öffnet Datenbankverbindung
//                Session
//                Html
//                Titel
//                Menu
//-----------------------------------------------------------------------------

function page_begin()
{
  global $config_login;
  global $toolbarentries;
  global $template_path;
  global $template_img_edit;
  
  $menuentriesright = array(
  array("_Abmelden", "?logout=1")
);

  $theme = $_SESSION['theme'] ?? 'default';
  $template_path = "template/$theme/";
  $template_img_edit = $template_path."img/edit.png";

  html_begin('pflanzen.winterhart.ch');
  title();
  echo '<div id="symbol"></div>';
  echo '<div id="toolbar">';
  menu($toolbarentries, "toolbar-menu", " » ");
  menu($menuentriesright, "toolbar-right-menu", " : ");
  echo '</div>';
  body_begin();
}

function page_end(): void {
  body_end();
  footer();
  html_end();
}

function title(): void {
  echo '<div id="header">';
  echo '<div class="title">';
  echo '<a class="title-one" href="index.php">pflanzen</a><a class="title-two" href="index.php">.winterhart</a><a class="title-one" href="index.php">.ch</a>';
  echo '</div>';  
  echo '</div>';  
}  

function footer(): void {
  global $config_version;
  echo '<footer id="footer">'.(isset($_SESSION['displayname'])?"Benutzer: ".$_SESSION['displayname']." - ":"")."Version $config_version - © 2005-2021 Winterhart Team</footer>";
}    

function body_begin(): void {
  echo '<div id="content">';
}

function body_end(): void {
  echo '</div>';
}

function session()
{
  global $toolbarentries;

  if(isset($_GET['logout'])) {
    session_unset();
	  session_destroy();
	  header('Location: index.php');
  }
  
  if(isset($_POST['form1_username']))
  {
    $data = db_sql("SELECT account.username, account.password, account.displayname, account.id, theme.text FROM account LEFT JOIN theme ON theme.id=account.theme_id WHERE username = '".$_POST['form1_username']."'", false);

    if($_POST['form1_username']==$data[0] && $_POST['form1_password']==$data[1])
    {
	  $_SESSION['displayname'] = $data[2];
      $_SESSION['username'] = $_POST['form1_username'];
      $_SESSION['userid'] = $data[3];
      $_SESSION['theme'] = $data[4];
    }
	else {
    unset($_SESSION['username']);
    unset($_SESSION['displayname']);
    unset($_SESSION['userid']);
    unset($_SESSION['theme']);
    }
  }
  
  if(!isset($_SESSION['username']))
  {
    $toolbarentries = array(array("Anmeldung", htmlentities($_SERVER['PHP_SELF'])));
    page_begin();
	page_title("Anmeldung");
?>
<p>
<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
<table>
<tr>
  <td style="width:10%">Benutzername</td>
  <td><input name="form1_username"></td>
</tr>
<tr>
  <td>Passwort</td>
  <td><input type="password" name="form1_password"></td>
</tr>
<tr>
  <td></td>
  <td><input type="submit" value="Anmeldung"></td>
</tr>
</table>
</form>
</p>
    <?php
	page_end();
    exit();
  }
}
?>