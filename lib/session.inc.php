<?php

function status_set()
{
  if (isset($_SESSION['$status'])) {
    $status = $_SESSION['$status'];
    if (isset($status[$_SERVER['PHP_SELF']]))
      $vars = $status[$_SERVER['PHP_SELF']];
   }

  foreach($_REQUEST as $key => $value)
    $vars[$key] = $value;

  if(isset($vars)) {
    $status[$_SERVER['PHP_SELF']] = $vars;
    $_SESSION['$status'] = $status;
  }
}

function status_set_key($key, $value)
{
  $status = $_SESSION['$status'];

  $vars = $status[$_SERVER['PHP_SELF']];

  $vars[$key] = $value;

  $status[$_SERVER['PHP_SELF']] = $vars;
  $_SESSION['$status'] = $status;
}

function status_reset() {
  unset($_SESSION['$status']);
}

function status_get($var)
{
  if (isset($_SESSION['$status'])) {
    $status = $_SESSION['$status'];
    if (isset($status[$_SERVER['PHP_SELF']])) {
      $vars = [$_SERVER['PHP_SELF']];
      if (isset($vars[$var]))
        return $vars[$var];
    }
  }
  return null;
}

function lastpage_set()
{
  if(!isset($_SESSION['$activepage']))
    $_SESSION['$activepage'] = $_SERVER['PHP_SELF'];
    
  if($_SESSION['$activepage']!=$_SERVER['PHP_SELF'])
  {
     $_SESSION['$lastpage'] = $_SESSION['$activepage'];
     $_SESSION['$activepage'] = $_SERVER['PHP_SELF'];
  }
}  

function lastpage_get()
{
  return $_SESSION['$lastpage'];
}

?>