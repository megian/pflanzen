<?php
//-----------------------------------------------------------------------------
// @library        function.inc.php
// @version        1.2
// @date           2.6.2003
// @update         9.5.2006
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
// @licence        GPL
//-----------------------------------------------------------------------------
// Liddle CMS - General PHP-Functions
// Copyright (C) 2003-2005 Gabriel Mainberger <gabisoft@freesurf.ch>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//------------------------------------------------------------------------------
//
// History:
//
// 26.10.2003 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - First Publicated Version
// 03.02.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - XHTML 1.1 migration
// 24.12.2005 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Funktion menu
//   - Support for UTF-8 database connection
// 17.12.2021 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - HTML5 migration
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Deutsche Monatsnamen in einem Array
//-----------------------------------------------------------------------------

$monthname = array("", "Januar", "Februar", "März", "April", "Mai", "Juni",
"Juli", "August", "September", "Oktober", "November", "Dezember");

//-----------------------------------------------------------------------------
// @function        errormsg()
// @paramter        Fehlermeldung
// @return        nichts
// @description        Gibt eine Fehlermeldung aus und bricht die Ausführung
//                der Script Ausführung ab.
//-----------------------------------------------------------------------------

function errormsg($msg)
{
  html_begin();
  echo "ERROR: $msg";
  html_end();
  exit;
}

//-----------------------------------------------------------------------------
// @function        warnmsg()
// @paramter        Warnung
// @return        nichts
// @description        Gibt eine Warnmeldung aus und führt das Script weiter aus.
//-----------------------------------------------------------------------------

function warnmsg($msg)
{
  echo $msg;
}

//-----------------------------------------------------------------------------
// @function        html_begin()
// @paramter        Tiel der Seite (optional)
// @return        nichts
// @description        Erstellt den Grundaufbau der HTML Seite (urt-8, ISO-8859-1)
//-----------------------------------------------------------------------------

function html_begin(string $title="Noname", string $css=""): void  {
  global $template_path;
  
  if($css=="")
    $css = $template_path."/default.css";

  header("Content-type: text/html; charset=utf-8"); 
  echo <<<___HTML___
  <!DOCTYPE html>
  <html lang="de">
  <head>
    <meta charset="utf-8">
    <title>$title</title>
    <meta name="author" content="Winterhart Team">
    <meta name="keywords" content="winterhart, pflanzendatenbank, pflanzen">
    <meta name="description" content="A plant database.">
    <link rel="stylesheet" title="Default Style" type="text/css" href="$css">
  </head>
  <body>
  ___HTML___;
}

//-----------------------------------------------------------------------------
// @function        html_end()
// @paramter        nichts
// @return        nichts
// @description        Beendet die HTML Seite
//-----------------------------------------------------------------------------

function html_end(): void {
  echo '</body></html>';
}

//-----------------------------------------------------------------------------
// @function        db_open()
// @paramter        nichts
// @return        nichts
// @description Stellt die Verbindung zur Datenbank her und setzt die
//                Standard Datenbank. Die Paramter die benötigt werden, werden
//                in der config.inc.php konfiguriert.
//                Der Datenbank-Handle ist in der globalen Variable
//                $mysql_connect_handle vorhanden.
//-----------------------------------------------------------------------------

function db_open()
{
  global $config_mysql_hostname;
  global $config_mysql_username;
  global $config_mysql_password;
  global $config_mysql_database;
  global $mysql_connect_handle;

  $mysql_connect_handle = mysqli_connect($config_mysql_hostname, $config_mysql_username, $config_mysql_password, $config_mysql_database);
  
  if(mysqli_connect_errno())
    errormsg("mysqli_connect(): Konnte Datenbankverbindung nicht herstellen!" + mysqli_connect_error());

  /* change character set to utf8 */
  if (!mysqli_set_charset($mysql_connect_handle, "utf8"))
  {
    printf("Error loading character set utf8: %s\n", mysqli_error($mysql_connect_handle));
  }
}

//-----------------------------------------------------------------------------
// @function        db_sql()
// @paramter        SQL
// @return        Array mit allen Daten
// @description        Fragt mit einem SQL Befahl die Datenbank ab und gibt das
//                Resultat in einem Arry zurück.
//-----------------------------------------------------------------------------

function db_sql($sql, $debug=true)
{
  global $mysql_connect_handle;

  if(!$result = mysqli_query($mysql_connect_handle, $sql))
    die('Konnte SQL-Statement nicht ausfuehren!');

  if($row = mysqli_fetch_row($result))
  {
    mysqli_free_result($result);
    return($row);
  }
  else
    warnmsg("db_sql(): Konnte SQL-Statement nicht ausfuehren!");
}

//-----------------------------------------------------------------------------
// @function        db_sql_multi()
// @paramter        SQL, Aufzurufende-Funktion
// @return        nichts
// @description        Fragt mit einem SQL Befahl die Datenbank ab und ruft für jeden
//                Datensatz die angegebene Funktion auf.
//-----------------------------------------------------------------------------

function db_sql_multi($sql, $func)
{
  global $mysql_connect_handle;

  $result = mysqli_query($mysql_connect_handle, $sql);

  while($row = mysqli_fetch_row($result))
    $func($row);

  mysqli_free_result($result);
}

function menu($menuentries, $htmlclass="menu", $seperator=" ") {
  echo "<div class=\"$htmlclass\">";
  $count = count($menuentries);
  $n = 0;
  foreach($menuentries as $arr) {
    $n++;
    $caption = $arr[0];
    $index = strpos($caption, "_");

    if(isset($arr[2]))
      $active = ' style="background-color:#fcffbb;"';
    else
    $active = '';

    $accesskey = '';
    if(strstr($caption, "_")!=FALSE)
    {
      $key = $caption[$index+1];
      $accesskey = " accesskey=\"".strtolower($key)."\"";
      $caption = str_replace("_$key", "<span class=\"accesskey\">$key</span>", $caption);
    }
    echo "<a href=\"".$arr[1]."\"$accesskey$active>".$caption."</a>";
	if($n<$count) echo $seperator;
  }
  echo '</div>';
}

function sqltable_begin($columns) {
  echo '<table class="sqltable">';
  echo '<tr>';
  
  foreach($columns as $arr) {
    if(isset($arr[1]))
      echo '<th style="width:'.$arr[1].'">'.$arr[0].'</th>';
    else
      echo '<th>'.$arr[0].'</th>';
  }
  echo '</tr>';
}

function sqltable_end(): void {
  echo '</table>';
}

function page_title($title): void {
  echo "<h1>$title</h1>";
}

function db_limit_navigation($form, $sql, $limit)
{
  global $mysql_connect_handle;

  $result = mysqli_query($mysql_connect_handle, $sql);
  $nrows = mysqli_num_rows($result);
  mysqli_free_result($result);

  $rows = (int)($nrows / $limit);

  if($rows>0)
  {
    echo "<p class=\"menu\">";
    for($i=0;$i<=$rows;$i++)
    {
      $active="";
      if(status_get($form."o")==$i)
	    $active = " style=\"background-color:#fcffbb;\"";
      echo "<a ".$active."href=\"?".$form."o=$i\">".($i+1)."</a> ";
    }
    echo "</p>";
  }
}

function db_limit($form, $limit)
{
  return " LIMIT ".(status_get($form."o")*$limit).",".$limit;
}

?>