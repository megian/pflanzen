<?php
//-----------------------------------------------------------------------------
// @library        form.inc.php
// @version        1.0
// @date           20.7.2003
// @update         18.01.2004
// @authors        Gabriel Mainberger <gabisoft@freesurf.ch>
// @licence        GPL
//-----------------------------------------------------------------------------
// Liddle CMS - Form Designer
// Copyright (C) 2003-2005 Gabriel Mainberger <gabisoft@freesurf.ch>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//------------------------------------------------------------------------------
//
// History:
//
// 26.10.2003 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - First Publicated Version
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "smart" to db_form()
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "select" to db_form()
// 18.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "file" to db_form()
// 21.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Fix </text> to </textarea>
// 25.01.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "idfile" to db_form()
// 09.02.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - XHTML 1.1 compatible code
// 14.02.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "htmlarea" to db_form()
// 16.07.2004 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add type "heckbox" to db_form()
// 17.10.2005 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - reimplement for better code style ;-)
// 27.11.2005 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Add element add for insert for insert table only
// 29.01.2006 - Gabriel Mainberger <gabisoft@freesurf.ch>
//   - Change to UTF-8
//
//------------------------------------------------------------------------------

// Feldtyp, Bezeichnung, DB-Name, default, grösse, maximal

function db_form2($formname, $table, $fields)
{
  global $id;
  global $mysql_connect_handle;

  $add_special_id = 0;

  if(!isset($id))
   $id = status_get("id");

  if(isset($_POST[$formname.'_del']))
    unset($id);

  echo '<form accept-charset="utf-8" method="post" enctype="multipart/form-data" action="'.htmlentities($_SERVER['PHP_SELF']).'">';

  if(isset($id))
  {
    $result = mysqli_query($mysql_connect_handle, "SELECT id FROM $table WHERE id = $id");
    $rows = mysqli_num_rows($result);
    mysqli_free_result($result);
    if($rows==1)
    {
      echo '<input type="hidden" name="'.$formname.'_mod" value="'.$id.'">';
      if(isset($id))
      {
        $sql = 'SELECT ';

        foreach($fields as $field) {
          // Initalize
          $field_type = $field['type'];
          $field_name = $field['name'] ?? '';

          $valid_fields = [
            'text',
            'textarea',
            'password',
            'select',
            'file',
            'idfile',
            'htmlarea',
            'checkbox',
            'seq'
          ];

          if(in_array($field_type, $valid_fields)) {
            if($field !== reset($fields))
              $sql .= ', ';

            $sql .= $field_name;
          }
        }

        $sql .= " FROM $table WHERE id = $id";

        echo 'k'.$sql;

        $data = db_sql($sql, $mysql_connect_handle);
      }
    } else {
      echo '<input type="hidden" name="'.$formname.'_add" value="'.$id.'">';
      $add_special_id = 1;
    }
  }
  else
    echo '<input type="hidden" name="'.$formname.'_add" value="0">';

  $d = 0;
  foreach($fields as $field) {
    // Initalize
    $field_type = $field['type'];
    $field_label = $field['label'] ?? '';
    $field_name = $field['name'] ?? '';
    $field_default = $field['defaultvalue'] ?? '';
    $field_var1 = $field['maxlength'] ?? '';
    $field_var2 = $field['size'] ?? '';

    if($field_type == 'hidden') {
      echo '<input type="hidden" name="'.$formname.'_'.$field_name.'" value="'.$field_default.'">';
      continue;
    }

    if($field_type == 'silent') {
      echo '<input type="hidden" name="'.$field_name.'" value="'.$field_default.'">';
      continue;
    }

    if(($field_type == 'add') && (!isset($id))) {
      echo '<input type="hidden" name="'.$formname.'_'.$field_name.'" value="'.$field['value'].'">';
      continue;
    }

    if($field_type == 'seq') {
      echo '<input type="hidden" name="'.$formname.'_seq" value="'.(isset($id)?(($data[$d++] ?? 0)+1):"1").'">';
      continue;
    }

    if(isset($field['label']))
      echo '<label>'.$field['label'].'</label>';

    if(($field_type == 'text') or ($field_type == 'password'))
      echo '<input type="'.$field_type.'" name="'.$formname.'_'.$field_name.'" size="'.$field['size'].'" maxlength="'.$field['maxlength'].'"  value="'.(isset($id)&&$add_special_id==0?$data[$d++]:$field_default).'">';
    else if($field_type == 'textarea')
      echo '<textarea name="'.$formname.'_'.$field_name.'" cols="'.$field['cols'].'" rows="'.$field['rows'].'">'.(isset($id)&&$add_special_id==0?$data[$d++]:$field_default).'</textarea>';
    else if($field_type == 'htmlarea')
      echo '<textarea id="ta" name="ta" cols="'.$field_var1.'" rows="'.$field_var2.'" style="width: 100%">'.(isset($id)&&$add_special_id==0?$data[$d++]:$field_default)."</textarea>";
    else if($field_type == 'checkbox')
    {
      if($data[$d++] == '1')
        echo '<input type="checkbox" name="'.$formname."_".$field_name.'" checked>';
      else
        echo '<input type="checkbox" name="'.$formname."_".$field_name.'">';
    }
    else if($field_type == 'select') {
      $selectedid = $data[$d++] ?? 0;
      echo '<select name="'.$formname.'_'.$field_name.'" size="1">';
      $result = mysqli_query($mysql_connect_handle, $field['optionsql']);
      while($row = mysqli_fetch_row($result)) {
        $disabled = '';
        $default = '';
        if($row[0] == 0) {
          $disabled = ' disabled';
          $default = ' -- ';
        }

        $selected = '';
        if($row[0] == $selectedid)
          $selected = ' selected';

          echo '<option'.$disabled.$selected.' value="'.$row[0].'">'.$row[1].$default.'</option>';
      }
      mysqli_free_result($result);
      echo '</select>';
    }
    else if($field_type == 'file')
    {
      if(isset($id))
        echo '<a href="'.$field_var1.$data[$d].'">'.$data[$d++].'</a>';
      else
      {
        echo '<input type="file" name="'.$formname.'_'.$field_name.'">';
        $d++;
      }
    }
    else if($field_type == 'idfile')
    {
      if(isset($id))
      {
        if($data[$d] != '')
          $ext = '.'.$data[$d++];
        echo '<a href="'.$field['path'].$id.$ext.'">'.$id.$ext.'</a>';
      }
      else
      {
        echo '<input type="file" name="'.$formname.'_'.$field_name.'" size="80">';
        $d++;
      }
    } else if($field_type == 'echo') {
      echo $field['html'];
    }
  }

  if(isset($id) && $add_special_id==0)
  {
    echo '<input type="submit" value="Ändern" accesskey="s"></form>';

    echo '<form method="post" action="'.htmlentities($_SERVER['PHP_SELF']).'">';
    echo '<input type="hidden" name="'.$formname.'_del" value="'.$id.'">';
    echo '<input type="submit" value="Löschen">';
    echo '</form>';
  }
  else
    echo '<input type="submit" value="Einfügen" accesskey="s"></form>';
}

function db_add2($formname, $table, $fields, $text)
{
  global $id;
  global $mysql_connect_handle;

  $valid_fields = [
    'text',
    'textarea',
    'password',
    'hidden',
    'select',
    'file',
    'idfile',
    'htmlarea',
    'checkbox',
    'seq',
    'add',
    'modtime',
    'instime',
  ];

  // Check if $formname_add is set
  if(!isset($_POST[$formname.'_add']))
    return;

  $sql = "INSERT INTO $table (";

  if($_POST[$formname.'_add'] != '0') $sql .= 'id, ';

  foreach($fields as $field) {
    if(in_array($field['type'], $valid_fields)) {
      if($field !== reset($fields))
        $sql .= ', ';

      $sql .= $field['name'];
    }
  }

  $sql .= ') VALUES (';

  if($_POST[$formname.'_add'] != '0') $sql .= $id.', ';

  foreach($fields as $field) {
    $field_type = $field['type'];
    $field_name = $field['name'];

    if(in_array($field_type, $valid_fields)) {
      if($field !== reset($fields))
        $sql .= ", ";

        $sql .= match($field_type) {
          'file' => "'".$_FILES[$formname."_".$field_name]["name"]."'",
          'idfile' => "'".extractfileext($_FILES[$formname."_".$field_name]["name"])."'",
          'htmlarea' => "'".$_POST[ta]."'",
          'checkbox' => $_POST[$formname."_".$field_name] == 'on' ? "'1'" : "'0'",
          'modtime' => 'NOW()',
          'instime' => 'NOW()',
          default => "'".$_POST[$formname."_".$field_name]."'",
        };
    }
  }

  $sql .= ")";

  echo $sql;

  //if(!mysqli_query($mysql_connect_handle, $sql))
  //  errormsg("Konnte Daten nicht in die Datenbank eintragen!");

  if($text!="")
  {
    echo $text;
    footer();
    page_end();
    exit;
  }

  //if(isset($_POST[$formname.'_add']))
  //  if($_POST[$formname.'_add']=="0")
  //    $id = mysqli_insert_id($mysql_connect_handle);

  foreach($fields as $field) {
    if($field['type'] == 'file')
    move_uploaded_file($_FILES[$formname."_".$field['name']]["tmp_name"], $field[4].$_FILES[$formname."_".$field['name']]["name"]);
    if($fields[$i]['type'] == 'idfile') {
      $ext = extractfileext($_FILES[$formname."_".$field['name']]["name"]);
      if($ext != "")
        $ext = ".$ext";

      move_uploaded_file($_FILES[$formname."_".$field['name']]["tmp_name"], $field[4].$id.$ext);
    }
  }

}

function db_add_tmb2($formname, $fields, $dir)
{
  global $id;

  // Check if $formname_add is set
  if(!isset($_POST[$formname.'_add']))
    return;

  foreach($fields as $field) {
    $field_type = $field['type'];

    // Check if file type is idfile
    if($field_type != "idfile")
      continue;

    $field_name = $field['name'];
    $field_filename = $field['filename'];

    $ext = extractfileext($_FILES[$formname."_".$field_name ]["name"]);
    if($ext=="jpg" || $ext=="png" || $ext=="gif") {
      //echo $field[4].$id.".".$ext;
      thumbjpg($field[4].$id.".".$ext, $dir.$id.".jpg", 150, 150, $quality=40);
    }
  }

}

function db_mod2(string $formname, string $table, array $fields): void {
  global $id;
  global $mysql_connect_handle;

  // Check if $formname_mod is set
  if(!isset($_POST[$formname.'_mod']))
    return;

  $sql = "UPDATE $table SET ";

  foreach($fields as $field) {
    $field_type = $field['type'];
    $field_name = $field['name'];
    $valid_fields = [
      'text',
      'textarea',
      'password',
      'hidden',
      'select',
      'htmlarea',
      'checkbox',
      'seq',
      'modtime',
    ];

    // Check if the field type is valid
    if(!in_array($field_type, $valid_fields))
      continue;

    if($field !== reset($fields))
      $sql .= ', ';

    $sql .= $field_name.' = ';
    $sql .= match($field_type) {
      'htmlarea' => "'".$_POST['ta']."'",
      'modtime' => 'NOW()',
      'checkbox' => $_POST[$formname."_".$field_name] == 'on' ? "'1'" : "'0'",
      default => "'".$_POST[$formname."_".$field_name]."'",
    };
  }

  $sql .= " WHERE id = ".$_POST[$formname.'_mod'];

  //echo $sql;

  if(!mysqli_query($mysql_connect_handle, $sql))
    errormsg("Konnte Daten nicht ändern!");

  $id = $_POST[$formname.'_mod'];
}

?>