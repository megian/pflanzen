<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Pflanze", "mainpage.php"),
  array("_Substrate", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = 'substrate';

$fields = [
  ...$fields_defaults_main,
  [
    'type' => 'text',
    'name' => 'name',
    'label' => 'Name',
    'size' => 50,
    'maxlength' => 50,
  ],
  [
    'type' => 'select',
    'name' => 'success_id',
    'label' => 'Erfolg',
    'optionsql' => 'SELECT id, text FROM percent',
  ],
];

db_add2('form1', $table, $fields, '');
db_mod2('form1', $table, $fields);
db_del('form1', $table);
db_back('form1', 'mainpage.php');

page_begin();
page_title('Substrat');
db_form2('form1', $table, $fields);

if(status_get("id")!="")
{

$menuentries = array(
  array("_Hinzufügen", "substratepartsform.php?status=0&substrate_id=".status_get("id"))
);
  echo "<br>";
  menu($menuentries);
  
$sqltablecolumns = array(
  array("", "5%"),
  array("Art"),
  array("Anteil")
);
  sqltable_begin($sqltablecolumns);
  
  $table = "substrateparts";

  function show($data)
  {
    global $template_img_edit;
    echo "<tr onClick=\"location.href='substratepartsform.php?id=$data[0]'\"><td><a href=\"substratepartsform.php?id=$data[0]\"><img src=\"$template_img_edit\" alt=\"Edit\"></a></td><td>$data[1]</td><td>$data[2]</td></tr>";
  }

  db_sql_multi("SELECT $table.id, $table.text, percent.text FROM $table LEFT JOIN percent ON $table.percent_id = percent.id WHERE $table.substrate_id=".status_get("id"), 'show');

  sqltable_end();
}

page_end();
?>