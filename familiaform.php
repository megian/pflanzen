<?php

$toolbarentries = array(
  array("H_aupstamm", "index.php"),
  array("_Familien", $_SERVER['PHP_SELF'])
);

require('default.inc.php');

$table = "familia";

$fields = [
  ...$fields_defaults,
  [
    'type' => 'text',
    'name' => 'text',
    'label' => 'Familie',
    'size' => 50,
    'maxlength' => 50,
  ],
];

db_add2('form1', $table, $fields, '');
db_mod2('form1', $table, $fields);
db_del('form1', $table);
db_back('form1', lastpage_get());

page_begin();
page_title('Familie');
db_form2('form1', $table, $fields);

page_end();
?>